// Soal 1
console.log("--Soal 1--");

console.log("LOOPING PERTAMA");
var i = 0;
while (i < 20) {
    i += 2;
    console.log(i, "- I love coding");

}

console.log("LOOPING KEDUA");
//nilai i sebelumnya adalah 20
while (i > 0) {
    console.log(i, "- I will become a mobile developer");
    i -= 2;
}

// Soal 2
console.log("\n--Soal 2--");
var totalBaris = 20;
for (var i = 1; i <= totalBaris; i++) {
    if (i % 2 == 1) //jika ganjil
        if (i % 3 == 0) //jika ganjil dan kelipatan 3
            console.log(i, "I Love Coding");
        else //jika ganjil namun bukan kelipatan 3
            console.log(i, "Santai");
    else //jika genap
        console.log(i, "Berkualitas");
}

// Soal 3
console.log("\n--Soal 3--");
var baris = 4, kolom = 8;
for (var i = 0; i < baris; i++) {
    var strcat = "";
    for (var j = 0; j < kolom; j++) {
        strcat += "#";
    }
    console.log(strcat);
}

// Soal 4
console.log("\n--Soal 4--");
var baris = 7;
for (var i = 1; i <= baris; i++) {
    var strcat = "";
    for (var j = 1; j <= i; j++) {
        strcat += "#";
    }
    console.log(strcat);
}

// Soal 5
console.log("\n--Soal 5--");
var baris = 8, kolom = 8;
for (var i = 0; i < baris; i++) {
    var strcat = "";
    for (var j = 0; j < kolom; j++) {
        if ((i + j) % 2 == 0) strcat += " ";
        else strcat += "#";
    }
    console.log(strcat);
}