// Soal 2
console.log("--Soal 2--")
var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

var waktu = 10000
function mulaiBaca(x) {
    if (x == books.length) {
        return 0;
    }
    readBooksPromise(waktu, books[x])
        .then(function (fulfilled) {
            // console.log(fulfilled)
            waktu -= books[x].timeSpent
            mulaiBaca(x + 1)
        })
        .catch(function (error) {
            // console.log(error)
        })

}
mulaiBaca(0)