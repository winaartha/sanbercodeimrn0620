// Soal 1
console.log("--Soal 1--")
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

var waktu = 10000
function mulaiBaca(x) {
    if (x == books.length) {
        return 0;
    }
    readBooks(waktu, books[x], function (check) {
        waktu -= books[x].timeSpent
        mulaiBaca(x + 1)
    })

}
mulaiBaca(0)