// Soal 1
console.log("--Soal 1--")
function range(x, y) {
    var hasil = []
    if (x === undefined || y === undefined)
        return -1;
    else if (x <= y) {
        for (var i = x; i <= y; i++)
            hasil.push(i)
    } else {
        for (var i = x; i >= y; i--)
            hasil.push(i)
    }
    return hasil;
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

// Soal 2
console.log("\n--Soal 2--")
function rangeWithStep(x, y, z = 1) {
    var hasil = []
    if (x === undefined || y === undefined)
        return -1;
    else if (x <= y) {
        for (var i = x; i <= y; i += z)
            hasil.push(i)
    } else {
        for (var i = x; i >= y; i -= z)
            hasil.push(i)
    }
    return hasil;
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

// Soal 3
console.log("\n--Soal 3--")
function sum(x = 0, y = x, z = 1) {
    var hasil = 0
    if (x <= y) {
        for (var i = x; i <= y; i += z)
            hasil += i
    } else {
        for (var i = x; i >= y; i -= z)
            hasil += i
    }
    return hasil;
}
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

// Soal 4
console.log("\n--Soal 4--")
function dataHandling(data) {
    for (var i = 0; i < data.length; i++) {
        console.log("Nomor ID:  " + data[i][0])
        console.log("Nama Lengkap:  " + data[i][1])
        console.log("TTL:  " + data[i][2], data[i][3])
        console.log("Hobi:  " + data[i][4])
        console.log("")
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input);

// Soal 5
console.log("\n--Soal 5--")
function balikKata(kata) {
    var hasil = []
    for (var i = kata.length - 1; i >= 0; i--) {
        hasil.push(kata[i]);
    }
    return hasil.join("")
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

// Soal 6
console.log("\n--Soal 6--")

function namaBulan(tanggal) {
    var strBulan = "";
    switch (Number(tanggal[1])) {
        case 1:
            strBulan = "Januari"; break;
        case 2:
            strBulan = "Februari"; break;
        case 3:
            strBulan = "Maret"; break;
        case 4:
            strBulan = "April"; break;
        case 5:
            strBulan = "Mei"; break;
        case 6:
            strBulan = "Juni"; break;
        case 7:
            strBulan = "Juli"; break;
        case 8:
            strBulan = "Agustus"; break;
        case 9:
            strBulan = "September"; break;
        case 10:
            strBulan = "Oktober"; break;
        case 11:
            strBulan = "November"; break;
        case 12:
            strBulan = "Desember"; break;
    }
    return strBulan;
}

function dataHandling2(data) {
    data[1] = data[1] + " Elsharawy"
    data[2] = "Provinsi " + data[2]
    data.splice(4, 1)
    data.splice(4, 0, "Pria")
    data.splice(5, 0, "SMA Internasional Metro")
    console.log(data)

    var tanggal = data[3].split("/")
    var tanggal2 = tanggal.slice()
    console.log(namaBulan(tanggal))

    tanggal.sort(function (a, b) { return parseInt(b) - parseInt(a) })
    console.log(tanggal)


    console.log(tanggal2.join("-"))

    console.log(String(data[1]).slice(0, 14))
}

dataHandling2(["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"])
