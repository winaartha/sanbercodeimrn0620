// Soal 1
console.log("--Soal 1--")
function arrayToObject(arr) {
    if (arr === undefined || arr.length == 0)
        console.log("")
    else {
        var now = new Date()
        var thisYear = now.getFullYear()

        for (var i = 0; i < arr.length; i++) {
            var tahun = arr[i][3]
            var object = {
                firstName: arr[i][0],
                lastName: arr[i][1],
                gender: arr[i][2],
                age: (tahun === undefined || tahun > thisYear) ? "Invalid Birth Year" : thisYear - tahun
            }
            console.log(i + 1 + ".", object.firstName, object.lastName, object)
        }
    }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)

// Error case 
arrayToObject([]) // ""

// Soal 2
console.log("\n--Soal 2--")
function shoppingTime(memberId, money) {
    if (memberId == null || memberId == '')
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    else {
        var listBarang = [{ label: "Sepatu Stacattu", harga: 1500000 }, { label: "Baju Zoro", harga: 500000 }, { label: "Baju H&N", harga: 250000 }, { label: "Sweater Uniklooh", harga: 175000 }, { label: "Casing Handphone", harga: 50000 }]

        if (money < 50000)
            return "Mohon maaf, uang tidak cukup"
        else {
            var object = {
                memberId: memberId,
                money: money,
                listPurchased: [],
                changeMoney: 0
            }
            var i = 0
            while (i < listBarang.length && money >= 50000) {
                if (listBarang[i].harga <= money) {
                    object.listPurchased.push(listBarang[i].label)
                    money -= listBarang[i].harga
                }
                i++
            }
            object.changeMoney = money
        }

        return object
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3
console.log("\n--Soal 3--")
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    harga = 2000
    var listPenumpang = []
    for (var i = 0; i < arrPenumpang.length; i++) {
        var angkot = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: Math.abs(rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * harga
        }
        listPenumpang.push(angkot)
    }
    return listPenumpang
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));

console.log(naikAngkot([])); //[]